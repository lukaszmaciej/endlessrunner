﻿using UnityEngine;
using Unity.RemoteConfig;
using UnityEngine.Events;
public class RemoteConfigsManager : MonoBehaviour
{
    public struct userAttributes { }
    public struct appAttributes { }

    public static UnityAction<int> OnLifesChange;
    void Awake()
    {
        FetchRemoteConfiguration();
    }

    public void FetchRemoteConfiguration()
    {
        if (gameObject.active)
        {
            ConfigManager.FetchCompleted += ApplyRemoteSettings;
            ConfigManager.FetchConfigs<userAttributes, appAttributes>(new userAttributes(), new appAttributes());
            Debug.Log("Fetched complete");
        }
    }
    void ApplyRemoteSettings(ConfigResponse configResponse)
    {
        switch (configResponse.requestOrigin)
        {
            case ConfigOrigin.Default:
                Debug.Log("No Setting Loaded this session; using default values");
                break;
            case ConfigOrigin.Cached:
                Debug.Log("No settings loaded this session; using cached values from a previous session.");
                break;
            case ConfigOrigin.Remote:
                Debug.Log("New Settings loaded this session; update values accordingly");

                SetPlayerSettings();
                break;
        }
    }
    void SetPlayerSettings()
    {
        int playerTotalLifePoints = ConfigManager.appConfig.GetInt("playerSetting_lifePoints");
        float playerMovemenetSpeed = ConfigManager.appConfig.GetFloat("playerSetting_movementSpeed");
        float playerSlideSpeed = ConfigManager.appConfig.GetFloat("playerSetting_slideSpeed");

        GameController.Instance.SetRemoteConfigurationToSettings(playerTotalLifePoints);
        OnLifesChange(playerTotalLifePoints);
        PlayerController.Instance.SetRemoteConfigurationToSettings(playerMovemenetSpeed, playerSlideSpeed);
    }
}
