﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;

    public static float movementSpeed = 3f;
    public static float maxMovementSpeed = 6f;
    [SerializeField]
    Camera cam;
    [SerializeField]
    float jumpHeight;
    [SerializeField]
    float jumpAcceleration;
    bool jump;
    Coroutine jumpingCoroutine;
    Coroutine changingTrackCoroutine;

    int track = 0;
    float tracksDistance = 4f;
    [SerializeField]
    float slideSpeed = 2f;

    private Touch theTouch;
    private Vector2 inputStartPosition;
    private Vector2 inputEndPosition;
    private string direction;
    float doubleTapTime = 0.2f;
    float screenWidthPercentage = 1f;
    float lastInputTime;
    public void Awake()
    {
        Instance = this;
    }
    private void Update()
    {
        direction = "";
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            inputStartPosition = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            inputEndPosition = Input.mousePosition;
            DetectSwipe();
        }
#elif UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount > 0)
        {
            theTouch = Input.GetTouch(0);

            if (theTouch.phase == TouchPhase.Began)
            {
                inputStartPosition = theTouch.position;
            }

            else if (theTouch.phase == TouchPhase.Moved || theTouch.phase == TouchPhase.Ended)
            {
                
                inputEndPosition = theTouch.position;
                DetectSwipe();
            }
        }
#endif
        if (direction == "Right")
        {
            if (changingTrackCoroutine == null)
            {
                changingTrackCoroutine = StartCoroutine(ChangeTrack(1));
            }
        }
        else if (direction == "Left")
        {
            if (changingTrackCoroutine == null)
            {
                changingTrackCoroutine = StartCoroutine(ChangeTrack(-1));
            }
        }
        if (Physics.Raycast(transform.position, Vector3.down, 0.6f))
        {
            if (jump && jumpingCoroutine == null)
            {
                jumpingCoroutine = StartCoroutine(Jump());
            }
        }
        else if (jumpingCoroutine == null)
        {
            jumpingCoroutine = StartCoroutine(Fall());
        }
        Vector3 cameraForward = cam.transform.forward;
        cameraForward.y = 0;
        transform.Translate(cameraForward * movementSpeed * Time.fixedDeltaTime);
    }

    private void DetectSwipe()
    {
        float x = inputEndPosition.x - inputStartPosition.x;
        float y = inputEndPosition.y - inputStartPosition.y;

        if (Mathf.Abs(x) < Screen.width * (screenWidthPercentage / 100f))
        {
            direction = "Tapped";
            DetectDoubleTap();
        }
        else if (Mathf.Abs(x) > Mathf.Abs(y))
        {
            if (Mathf.Abs(x) > Screen.width * (screenWidthPercentage / 100f))
                direction = x > 0 ? "Right" : "Left";
        }
        lastInputTime = Time.time;
    }

    private void DetectDoubleTap()
    {
        if (Time.time - lastInputTime < doubleTapTime)
        {
            jump = true;
        }
    }

    IEnumerator Jump()
    {
        Vector3 startPosition = transform.position;
        jump = false;
        while (transform.position.y < startPosition.y + jumpHeight)
        {
            transform.Translate(Vector3.up * jumpAcceleration * Time.fixedDeltaTime * Time.fixedDeltaTime);
            yield return null;
        }
        jumpingCoroutine = null;
    }
    IEnumerator Fall()
    {
        Vector3 startPosition = transform.position;
        while (!Physics.Raycast(transform.position, Vector3.down, 0.6f))
        {
            transform.Translate(-Vector3.up * jumpAcceleration * Time.fixedDeltaTime * Time.fixedDeltaTime);
            yield return null;
        }
        jumpingCoroutine = null;
    }
    IEnumerator ChangeTrack(int value)
    {
        if (track + value > 1 || track + value < -1)
        {
            changingTrackCoroutine = null;
            yield break;
        }
        track += value;
        Vector3 startPosition = transform.position;
        Vector3 endPosition = startPosition + Vector3.right * value * tracksDistance;
        while ((value == 1 && transform.position.x < endPosition.x) || (value == -1 && transform.position.x > endPosition.x))
        {
            transform.Translate(Vector3.right * slideSpeed * value * Time.fixedDeltaTime);
            yield return null;
        }
        changingTrackCoroutine = null;
    }
    public void SetRemoteConfigurationToSettings(float newMovementSpeed, float newSlideSpeed)
    {
        movementSpeed = newMovementSpeed;
        slideSpeed = newSlideSpeed;

    }
}
