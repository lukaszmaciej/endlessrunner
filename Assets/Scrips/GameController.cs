﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{
    public static GameController Instance;
    string bestScoreName = "bestScore";
    int points = 0;
    int lifes;
    int bestScore = 0;
    float startTime = 0;
    float time = 0;


    float speedIncrement = 0.5f;
    float speedIncreaseTimer;
    float speedUpWait = 60f;

    public static UnityAction<int> OnPointsChange;
    public static UnityAction<int> OnLifesChange;
    public static UnityAction<float> OnTimeChange;
    public static UnityAction<int> OnBestScoreChange;
    public static UnityAction OnGetHurt;

    [SerializeField]
    int nextScene;




    public void Awake()
    {
        startTime = Time.time;
        Instance = this;
        bestScore = PlayerPrefs.GetInt(bestScoreName, 0);
        Analytics.CustomEvent("newSession", new Dictionary<string, object>
        {
            { "time_elapsed", Time.timeSinceLevelLoad}
        });

    }
    public void Start()
    {
        speedIncreaseTimer = Time.time + speedUpWait;
    }
    void Update()
    {
        time = Time.time - startTime;
        time =+ Time.time;
        OnTimeChange(time);
        OnBestScoreChange(bestScore);
        if (speedIncreaseTimer < Time.time)
        {
            speedIncreaseTimer = Time.time + speedUpWait;

            increaseSpeed();
        }
        Debug.Log(PlayerController.movementSpeed);
    }
    void OnDisable()
    {
        if (points > bestScore)
        {
            PlayerPrefs.SetInt(bestScoreName, points);
            PlayerPrefs.Save();
            Analytics.CustomEvent("newRecord", new Dictionary<string, object>
        {
            {"time_passed", time },
            { "best_score", bestScore}
        });
        }
    }
    public void increaseSpeed()
    {
        if (PlayerController.movementSpeed == PlayerController.maxMovementSpeed)
            return;
        PlayerController.movementSpeed += speedIncrement;
    }
    public void increasePoints()
    {
        points++;
        OnPointsChange(points);
    }
    public void decreaseLifes()
    {
        lifes--;
        OnLifesChange(lifes);
        OnGetHurt();
        if (lifes == 0)
        {
            GameOver();
        }
    }
    public void GameOver()
    {
        if (points > bestScore)
        {
            PlayerPrefs.SetInt(bestScoreName, points);
            PlayerPrefs.Save();
        }
        Analytics.CustomEvent("gameOver", new Dictionary<string, object>
        {
            {"time_passed", time },
            { "potions", points }
        });
        time = 0;
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
    public void NextScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(nextScene);
    }
    public void SetRemoteConfigurationToSettings(int newLifePoints)
    {
        lifes = newLifePoints;
    }
}
