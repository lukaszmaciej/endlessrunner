﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform player;
    [SerializeField] private float gap = 4;
    public List<GameObject> spawned;
    public bool spawn = true;
    public float distance = 0;
    int objectToSpawn;
    void Start()
    {
        StartCoroutine(ObjectSpawner());
    }

    IEnumerator ObjectSpawner()
    {
        while (spawn)
        {
            if (Mathf.Abs(distance - player.transform.position.z) < 100)
            {
                distance += gap;
                GameObject obj = null;
                objectToSpawn = Random.Range(0, 3);
                switch (objectToSpawn)
                {
                    case 0:
                        obj = ObjectPooling.instance.GetObject("SampleSegmentOne");
                        break;
                    case 1:
                        obj = ObjectPooling.instance.GetObject("SampleSegmentTwo");
                        break;
                    case 2:
                        obj = ObjectPooling.instance.GetObject("SampleSegmentThree");
                        break;
                    case 3:
                        obj = ObjectPooling.instance.GetObject("SampleSegmentFour");
                        break;
                }
                spawned.Add(obj);
                obj.transform.position = new Vector3(0, 0, distance);
                distance += obj.transform.localScale.z;
                obj.SetActive(true);
            }
            yield return null;
        }
    }
}
