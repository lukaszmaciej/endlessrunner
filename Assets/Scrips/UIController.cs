﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
public class UIController : MonoBehaviour
{
    public TextMeshProUGUI lifesField;
    public TextMeshProUGUI pointsField;
    public TextMeshProUGUI bestField;
    public TextMeshProUGUI timeField;
    [SerializeField]
    GameObject damageFade;
    private void Awake()
    {
        GameController.OnPointsChange += OnPointsChange;
        RemoteConfigsManager.OnLifesChange += OnLifesChange;
        GameController.OnLifesChange += OnLifesChange;
        GameController.OnTimeChange += OnTimeChange;
        GameController.OnBestScoreChange += OnBestScoreChange;
        GameController.OnGetHurt +=  OnGetHurt;
    }

    private void OnDestroy()
    {
        GameController.OnPointsChange -= OnPointsChange;
        RemoteConfigsManager.OnLifesChange -= OnLifesChange;
        GameController.OnLifesChange -= OnLifesChange;
        GameController.OnTimeChange -= OnTimeChange;
        GameController.OnBestScoreChange -= OnBestScoreChange;
        GameController.OnGetHurt -= OnGetHurt;
    }

    public void OnPointsChange(int score)
    {
        pointsField.text = "Points: " + score.ToString();
    }
    public void OnLifesChange(int lifes)
    {
        lifesField.text = "Lifes: " + lifes.ToString();
    }
    public void OnTimeChange(float time)
    {
        timeField.text = "Time: " + TimeSpan.FromSeconds(time).ToString("mm\\:ss");
    }
    public void OnGetHurt()
    {
        damageFade.SetActive(true);
        Invoke("TurnOffDamageFade", 0.25f);
    }
    public void OnBestScoreChange(int bestScore)
    {
        bestField.text = "Best score: " + bestScore.ToString();
    }
    void TurnOffDamageFade()
    {
        damageFade.SetActive(false);
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
