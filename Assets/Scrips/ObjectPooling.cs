﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class OPItem
{
    public int amountPool;
    public GameObject objectPool;
    public bool shouldExpand;
}

public class ObjectPooling : MonoBehaviour
{
    public static ObjectPooling instance;
    public List<GameObject> pooledObjects;
    public List<OPItem> poolItems;

    public void Awake()
    {
        instance = this;
    }
    void Start()
    {
        pooledObjects = new List<GameObject>();
        foreach (OPItem item in poolItems)
        {
            for (int i = 0; i < item.amountPool; i++)
            {
                GameObject obj = (GameObject)Instantiate(item.objectPool);
                obj.SetActive(false);
                pooledObjects.Add(obj);
            }
        }
    }

    public GameObject GetObject(string tag)
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].tag == tag)
            {
                return pooledObjects[i];
            }
        }
        foreach (OPItem item in poolItems)
        {
            if (item.objectPool.tag == tag)
            {
                if (item.shouldExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.objectPool);
                    obj.SetActive(false);
                    pooledObjects.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }
}
